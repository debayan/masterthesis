import sys,os,json,re


gold = []
f = open('test.json')
d = json.loads(f.read())

for item in d:
    wikisparql = item['sparql_wikidata']
    unit = {}
    unit['uid'] = item['uid']
    _ents = re.findall( r'wd:(.*?) ', wikisparql)
    _rels = re.findall( r'wdt:(.*?) ',wikisparql)
    unit['entities'] = _ents
    unit['relations'] = _rels
    gold.append(unit)

f = open('tapioca1.json')
d = json.loads(f.read())
f.close()

tpentity = 0
fpentity = 0
fnentity = 0
tprelation = 0
fprelation = 0
fnrelation = 0
totalentchunks = 0
totalrelchunks = 0
mrrent = 0
mrrrel = 0
chunkingerror = 0
for queryitem,golditem in zip(d,gold):
    queryentities = []
    queryrelations = []
    if len(queryitem[1]) == 0:
        continue
    for annotation in queryitem[1]['annotations']:
        for tag in annotation['tags']:
            queryentities.append(tag['id'])
            break
    for goldentity in golditem['entities']:
        totalentchunks += 1
        if goldentity in queryentities:
            tpentity += 1
        else:
            fnentity += 1
#    for goldrelation in golditem['relations']:
#        totalrelchunks += 1
#        if goldrelation in queryrelations:
#            tprelation += 1
#        else:
#            fnrelation += 1
    for queryentity in queryentities:
        if queryentity not in golditem['entities']:
            fpentity += 1
    print(golditem['entities'], queryentities)
#    for queryrelation in queryrelations:
#        if queryrelation not in golditem['relations']:
#            fprelation += 1
precisionentity = tpentity/float(tpentity+fpentity)
recallentity = tpentity/float(tpentity+fnentity)
f1entity = 2*(precisionentity*recallentity)/(precisionentity+recallentity)
print("precision entity = ",precisionentity)
print("recall entity = ",recallentity)
print("f1 entity = ",f1entity)
#
