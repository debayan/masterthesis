from urllib.parse import urlencode
from urllib.request import Request, urlopen
import os,json,sys,re

url = 'https://opentapioca.org/api/annotate' # Set destination URL here

d = json.loads(open('test.json').read())
replies = []
for idx,item in enumerate(d):
    try:
        q = item['question']
        q = re.sub(r"[^a-zA-Z0-9]+", ' ', q)
        post_fields = {'query': q}
        request = Request(url, urlencode(post_fields).encode())
        request.get_method = lambda: 'POST'
        repjson = urlopen(request).read().decode()
        repdict = json.loads(repjson)
        print(idx, item['uid'], item['question'])
        replies.append([item['uid'], repdict])
    except Exception as e:
        print(e)
        replies.append([item['uid'], []])
f = open('tapioca1.json','w')
f.write(json.dumps(replies, indent=4, sort_keys=True))
f.close()
